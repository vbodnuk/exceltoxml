package excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;

public interface Read {
    public void readerExcel(String string) throws IOException, InvalidFormatException;
}

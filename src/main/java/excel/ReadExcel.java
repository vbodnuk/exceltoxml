package excel;

import models.Amount;
import models.Customer;
import models.Saller;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadExcel  implements Read {

    public static Map<Integer,List> doc = new HashMap<>();

    public void readerExcel(String string) throws IOException {
        FileInputStream f = new FileInputStream(new File(String.valueOf(string)));
        Workbook workbook = null;
        try {
            workbook = new WorkbookFactory().create(f);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        Sheet sheet = workbook.getSheetAt(0);
        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<String>());
            for (Cell cell : row) {
                switch (cell.getCellTypeEnum()) {
                    case STRING:
                        data.get(i).add(cell.getStringCellValue());
                        break;
                    case NUMERIC:
                        data.get(i).add(String.valueOf(cell.getNumericCellValue()));
                        break;
                }
            }
            i++;
        }
        workbook.close();


        for (int j = 1; j < data.size(); j++) {
            doc.put(j, new ArrayList());
            List<String> str = new ArrayList<>();
            Amount amount = new Amount();
            Customer customer = new Customer();
            Saller saller = new Saller();
            for (String value : data.get(j)) {
                str.add(value);
            }
            customer.setName(str.get(0));
            customer.setSureName(str.get(1));
            customer.setAge(str.get(2));
            customer.setLastActivity(str.get(3));
            saller.setFio(str.get(4));
            saller.setRating(str.get(5));
            amount.setPrice(str.get(6));
            amount.setPaymentMethod(str.get(7));
            amount.setPaymentSystem(str.get(8));
            doc.get(j).add(customer);
            doc.get(j).add(saller);
            doc.get(j).add(amount);


        }

        for(Map.Entry<Integer,List> map : doc.entrySet()){
            System.out.println(map.toString());
        }


    }
}


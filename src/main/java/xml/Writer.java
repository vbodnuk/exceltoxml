package xml;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public interface Writer {
    public void xmlWrite(String string) throws ParserConfigurationException, TransformerException;
}

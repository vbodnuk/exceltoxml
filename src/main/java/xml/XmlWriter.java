package xml;

import excel.ReadExcel;
import models.Amount;
import models.Customer;
import models.Saller;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;
import java.util.Map;

public class XmlWriter implements Writer{
    @Override
    public void xmlWrite(String string) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        Element rootElement = document.createElement("deposit");
        document.appendChild(rootElement);

        for (Map.Entry<Integer,List> map : ReadExcel.doc.entrySet()) {
            for (Object obj : map.getValue()){
                if(obj instanceof Customer){
                    Element customer = document.createElement("Customer");
                    rootElement.appendChild(customer);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(String.valueOf(map.getKey()));
                    customer.setAttributeNode(attr);

                    Element name = document.createElement("Name");
                    name.appendChild(document.createTextNode(((Customer) obj).getName()));
                    customer.appendChild(name);

                    Element sureName = document.createElement("SureName");
                    sureName.appendChild(document.createTextNode(((Customer) obj).getSureName()));
                    customer.appendChild(sureName);

                    Element age = document.createElement("Age");
                    age.appendChild(document.createTextNode(((Customer) obj).getAge()));
                    customer.appendChild(age);

                    Element lastActivity = document.createElement("LastActivity");
                    lastActivity.appendChild(document.createTextNode(((Customer) obj).getLastActivity()));
                    customer.appendChild(lastActivity);
                }
                if(obj instanceof Saller){
                    Element saller = document.createElement("Saller");
                    rootElement.appendChild(saller);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(String.valueOf(map.getKey()));
                    saller.setAttributeNode(attr);

                    Element fio = document.createElement("FIO");
                    fio.appendChild(document.createTextNode(((Saller) obj).getFio()));
                    saller.appendChild(fio);

                    Element rating = document.createElement("Rating");
                    rating.appendChild(document.createTextNode(((Saller) obj).getRating()));
                    saller.appendChild(rating);
                }
                if(obj instanceof Amount){
                    Element amount = document.createElement("Amount");
                    rootElement.appendChild(amount);

                    Attr attr = document.createAttribute("id");
                    attr.setValue(String.valueOf(map.getKey()));
                    amount.setAttributeNode(attr);

                    Element price = document.createElement("Price");
                    price.appendChild(document.createTextNode(((Amount) obj).getPrice()));
                    amount.appendChild(price);

                    Element paymentMethod = document.createElement("PaymentMethod");
                    paymentMethod.appendChild(document.createTextNode(((Amount) obj).getPaymentMethod()));
                    amount.appendChild(paymentMethod);

                    Element paymentSystem = document.createElement("PaymentSystem");
                    paymentMethod.appendChild(document.createTextNode(((Amount) obj).getPaymentSystem()));
                    amount.appendChild(paymentSystem);
                }
            }

        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT,"yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(string));
        transformer.transform(source,result);



    }
}

import excel.ReadExcel;
import xml.XmlWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@MultipartConfig
public class ServletUploadDownload extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream stream = resp.getOutputStream();
        Part part = req.getPart("file");
        String fileName = part.getName();
        String path = getServletContext().getInitParameter("uploadFile");
        if(part.getSize()== 0 ||fileName == null || fileName.equals("")){
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/upload.html");
            stream.println("<b color=red>File not found to upload!</b><br>");
            rd.include(req,resp);
        }else{
            InputStream is = part.getInputStream();
            Files.copy(is, Paths.get(path + fileName), StandardCopyOption.REPLACE_EXISTING);
            ReadExcel excel = new ReadExcel();
            excel.readerExcel(path + fileName);
            XmlWriter x = new XmlWriter();
            try {
                x.xmlWrite(path+fileName);
            } catch (ParserConfigurationException | TransformerException e) {
                e.printStackTrace();
            }
            resp.sendRedirect("download.html");
        }
   }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        String filename = "deposit.xml";
        String filepath = getServletContext().getInitParameter("uploadFile");
        resp.setContentType("APPLICATION/OCTET-STREAM");
        resp.setHeader("Content-Disposition", "attachment; filename=\""
                + filename + "\"");
        FileInputStream fileInputStream = new FileInputStream(filepath
                + filename);

        int i;
        while ((i = fileInputStream.read()) != -1) {
            out.write(i);
        }
        fileInputStream.close();
        out.close();
        }
}

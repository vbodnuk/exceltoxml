package models;

public class Customer {
    private String name;
    private String sureName;
    private String age;
    private String lastActivity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    @Override
    public String toString() {
        return "Castomer: " + name + " " + sureName + " " + age + " lastActivity: " + lastActivity;
    }
}

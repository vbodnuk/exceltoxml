package models;

public class Amount {
    private String price;
    private String paymentMethod;
    private String paymentSystem;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentSystem() {
        return paymentSystem;
    }

    public void setPaymentSystem(String paymentSystem) {
        this.paymentSystem = paymentSystem;
    }

    @Override
    public String toString() {
        return "Amount: " + "price " + price + ", paymentMethod: " + paymentMethod + ", paymentSystem: " + paymentSystem ;
    }
}

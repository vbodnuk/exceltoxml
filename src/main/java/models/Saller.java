package models;

public class Saller {
    private  String fio;
    private String rating;

    public  String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Saller: " + fio+ " rating: " + rating;
    }
}

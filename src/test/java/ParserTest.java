import excel.ReadExcel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import xml.XmlWriter;

import java.io.IOException;

public class ParserTest {

    private String xlsFile = "C:/Users/Vadym/Downloads/deposit.xlsx";
    private ReadExcel readExcel;
    private XmlWriter writer;

    @Before
    public void createdParser(){
        readExcel = new ReadExcel();
        writer = new XmlWriter();
    }
    @Test
    public void Test1() throws IOException {
        Assert.assertTrue(ReadExcel.doc.isEmpty());
        readExcel.readerExcel(xlsFile);
        Assert.assertFalse(ReadExcel.doc.isEmpty());
    }
    }



